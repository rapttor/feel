<html lang="en"><head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1">
    <title>Swiper Gallery App</title>
    <link rel="stylesheet" href="css/idangerous.swiper.css">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/gallery-app.css">
</head>
<body>
<div class="swiper-container" style="cursor: -webkit-grabbing;">
    <div class="pagination"><span class="swiper-pagination-switch"></span><span class="swiper-pagination-switch"></span><span class="swiper-pagination-switch"></span><span class="swiper-pagination-switch"></span><span class="swiper-pagination-switch"></span><span class="swiper-pagination-switch swiper-visible-switch"></span><span class="swiper-pagination-switch swiper-visible-switch swiper-active-switch"></span><span class="swiper-pagination-switch swiper-visible-switch"></span><span class="swiper-pagination-switch swiper-visible-switch"></span><span class="swiper-pagination-switch"></span><span class="swiper-pagination-switch"></span><span class="swiper-pagination-switch"></span></div>
    <div class="swiper-wrapper" style="padding-left: 389px; padding-right: 389px; transition: 0s; -webkit-transition: 0s; -webkit-transform: translate3d(-2650px, 0px, 0px); width: 5160px; height: 331px;">
        <div class="swiper-slide" style="width: 430px;">
            <div class="inner">
                <img src="img/gallery-2.jpg" alt="">
            </div>
        </div>
        <div class="swiper-slide" style="width: 430px;">
            <div class="inner">
                <img src="img/gallery-5.jpg" alt="">
            </div>
        </div>
        <div class="swiper-slide" style="width: 430px;">
            <div class="inner">
                <img src="img/gallery-6.jpg" alt="">
            </div>
        </div>
        <div class="swiper-slide" style="width: 430px;">
            <div class="inner">
                <img src="img/gallery-7.jpg" alt="">
            </div>
        </div>
        <div class="swiper-slide" style="width: 430px;">
            <div class="inner">
                <img src="img/gallery-2.jpg" alt="">
            </div>
        </div>
        <div class="swiper-slide swiper-slide-visible" style="width: 430px;">
            <div class="inner">
                <img src="img/gallery-5.jpg" alt="">
            </div>
        </div>
        <div class="swiper-slide swiper-slide-visible swiper-slide-active" style="width: 430px;">
            <div class="inner">
                <img src="img/gallery-6.jpg" alt="">
            </div>
        </div>
        <div class="swiper-slide swiper-slide-visible" style="width: 430px;">
            <div class="inner">
                <img src="img/gallery-7.jpg" alt="">
            </div>
        </div>
        <div class="swiper-slide swiper-slide-visible" style="width: 430px;">
            <div class="inner">
                <img src="img/gallery-2.jpg" alt="">
            </div>
        </div>
        <div class="swiper-slide" style="width: 430px;">
            <div class="inner">
                <img src="img/gallery-5.jpg" alt="">
            </div>
        </div>
        <div class="swiper-slide" style="width: 430px;">
            <div class="inner">
                <img src="img/gallery-6.jpg" alt="">
            </div>
        </div>
        <div class="swiper-slide" style="width: 430px;">
            <div class="inner">
                <img src="img/gallery-7.jpg" alt="">
            </div>
        </div>
    </div>
</div>
<script src="js/jquery.min.js"></script>

<script src="js/idangerous.swiper-2.0.min.js"></script>
<script src="js/gallery-app.js"></script>

</body></html>